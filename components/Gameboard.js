import React from "react";
import { Text, View, Pressable } from 'react-native';
import Entypo from "@expo/vector-icons/Entypo"
import styles from '../style/style'

let board = [];
const NBR_OF_ROWS = 5;
const NBR_OF_COLS = 5;
const START = 'plus';
const MISS = 'cross';
const HIT = 'circle';


const ships = []

function mix() {
    ships.shift()
    ships.shift()
    ships.shift()
    var rndNum = Math.floor(Math.random() * 25)
    var rndNum2 = Math.floor(Math.random() * 25)
    if (rndNum2 === rndNum) {
        var rndNum2 = Math.floor(Math.random() * 25)
    }
    var rndNum3 = Math.floor(Math.random() * 25)
    if ((rndNum3 === rndNum) || (rndNum3 === rndNum2)) {
        var rndNum3 = Math.floor(Math.random() * 25)
    }
    ships.push(rndNum, rndNum2, rndNum3);
    console.log(ships);
    
}

export default class Gameboard extends React.Component {
    timerId = 0;
    constructor(props) {
        super(props)
        this.state = {
            isCross: true,
            isShip: 24,
            started: false,
            status: 'Game has not started',
            gamestart: 'Start Game',
            time: 0,
            hits: 0,
            bombs: 15,
            left: 3,
            gameEnd: false,
        }
        this.standByBoard();
        
    }


    timeStart() {
            this.timerId = setInterval(() => {
                this.setState((prevState) => ({
                    time: prevState.time + 1
                }));
            }, 1000);
    }
    timeReset() {
        clearInterval(this.timerId);
    }
    

    standByBoard() {
        for (let i = 0; i < NBR_OF_ROWS * NBR_OF_COLS; i++) {
            board[i] = START;
        }
    }

    initializeBoard() {
        for (let i = 0; i < NBR_OF_ROWS * NBR_OF_COLS; i++) {
            board[i] = START;
        }
        mix();
        this.timeReset();
        this.timeStart();
    }


//          ----------  EN SAANUT TIMEOUTTIA TOIMIMAAN HALUTULLA TAVALLA, MUTTA LAITOIN    ----------
//          ----------  SOVELLUKSEN HYLKIMÄÄN YRITYKSIÄ KUN 30 SEKUNTIA ON KULUNUT         ----------
    
    
    winGame() {
        if (this.state.time > 29) {
            this.setState({
                gameEnd: true,
                status: 'Timeout. Ships remaining.'
            })
            this.timeReset();
        }

        if (this.state.bombs === 1) {
            this.setState({
                gameEnd: true,
                status: 'Game over. Ships remaining.'
            });
            this.timeReset();
        }
        if (this.state.left === 0) {
            this.setState({
                gameEnd: true,
                status: 'You sinked all ships'
            })
            this.timeReset();
        }
        console.log(this.state.gameEnd, this.state.bombs, this.state.hits);
        
    }



    drawItem(number) {

        if (this.state.started === false) {
            this.setState({
                status: 'Click the start button first...'
            })
            return;
        }
        let x = ships[0];
        let y = ships[1];
        let z = ships[2];
        
        

        if (board[number] === START && this.state.gameEnd === false) {

            if (this.state.time > 29) {
                this.winGame(); return;
            }
            
            if ((board[number] = number === x) || (board[number] = number === y) || (board[number] = number === z)) {
                board[number] = this.state.isCross = HIT,
                    this.setState({
                        isCross: !this.state.isCross,
                        bombs: (this.state.bombs = this.state.bombs - 1),
                        hits: (this.state.hits = this.state.hits + 1),
                        left: (this.state.left = this.state.left - 1),
                    }); this.winGame(); return;
                
                
                
                
            } else if ((board[number] = number !== x) || (board[number] = number !== y) || (board[number] = number !== z)){
                board[number] = this.state.isCross = MISS,
                    this.setState({
                        isCross: !this.state.isCross,
                        bombs: (this.state.bombs - 1),
                    }); this.winGame(); return;
                
                
                
            }
        } else {return}
        this.winGame();
    }

    chooseItemColor(number) {
        if (board[number] === MISS) {
            return "#FF3031"
        }
        else if (board[number] === HIT) {
            return "#45CE30"
        }
        else {
            return "#74B9FF"
        }
    }

    resetGame() {
        this.setState({
            isCross: true,
            status: 'Game is on...',
            gamestart: 'New Game',
            started: true,
            gameEnd: false,
            time: 0,
            hits: 0,
            bombs: 15,
            left: 3,

        })

        this.initializeBoard();
        <Text style={styles.buttonText}>New Game</Text>
    }


    render() {
        const items = [];
        for (let x = 0; x < NBR_OF_ROWS; x++) {
            const cols = [];
            for (let y = 0; y < NBR_OF_COLS; y++) {
                cols.push(
                    <Pressable
                        key={x * NBR_OF_COLS + y}
                        style={styles.item}
                        onPress={() => this.drawItem(x * NBR_OF_COLS + y)}>
                        <Entypo
                            key={x * NBR_OF_COLS + y}
                            name={board[x * NBR_OF_COLS + y]}
                            size={32}
                            color={this.chooseItemColor(x * NBR_OF_COLS + y)} />
                    </Pressable>
                );
            }
            let row =
                <View key={"row" + x}>
                    {cols.map((item) => item)}
                </View>
            items.push(row);
        }
        return (
            <View style={styles.gameboard}>
                <View style={styles.flex}>{items}</View>
                <Text style={styles.gameinfo}>Hits: {this.state.hits} Bombs: {this.state.bombs} Ships: {this.state.left} </Text>
                <Text style={styles.gameinfo}>Time: {this.state.time} sec</Text>
                <Text style={styles.gameinfo}>Status: {this.state.status}</Text>
                <Pressable style={styles.button} onPress={() => this.resetGame()}>
                    <Text style={styles.buttonText}>{this.state.gamestart}</Text>
                </Pressable>
            </View>
        )
    }
}